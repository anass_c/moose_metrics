# MIT License

# Copyright (c) 2020 Chaaraoui Anass

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import ast


class Constant:
    NOC = "NOC"
    DIT = "DIT"
    RFC = "RFC"
    WMC = "WMC"


class AttributeVisitor(ast.NodeVisitor):

    def __init__(self):
        self.funcCalls = set()

    def visit_Attribute(self, node):
        self.funcCalls.add(node.attr)


class FunctionVisitor(ast.NodeVisitor):

    def __init__(self):
        self.functionList = []

    def visit_FunctionDef(self, node):
        self.functionList.append(node.name)



class ClassVisitor(ast.NodeVisitor):

    def __init__(self, metric):
        self.res = {}
        self.metric = metric

    def visit_ClassDef(self, node):
        # first part to calculate the noc
        if node.name not in self.res:
            self.res[node.name] = 0
        if self.metric == Constant.NOC:
            for base in node.bases:
                if isinstance(base, ast.Attribute):
                    baseClass = base.attr
                elif isinstance(base, ast.Name):
                    baseClass = base.id

                if baseClass not in self.res:
                    self.res[baseClass] = 1
                else:
                    self.res[baseClass] = self.res[baseClass] + 1

        if self.metric == Constant.DIT:
            # if node.name not in self.res:
            #     self.res[node.name] = 0

            if len(node.bases) <= 0:
                self.res[node.name] = 0
            else:
                l = []
                for base in node.bases:

                    if isinstance(base, ast.Attribute):
                        baseClass = base.attr
                    elif isinstance(base, ast.Name):
                        baseClass = base.id
                    if baseClass not in self.res:
                        self.res[baseClass] = 0
                    l.append(self.res[baseClass])
                self.res[node.name] = 1 + max(l)

        if self.metric == Constant.RFC:
            fv = FunctionVisitor()
            fv.visit(node)
            # print(node.name+": "+str(len(fv.functionList)))
            # print(ast.dump(node), end="\n\n")
            av = AttributeVisitor()
            av.visit(node)
            # print(node.name+": "+str(len(av.funcCalls)))
            self.res[node.name] = len(fv.functionList)+len(av.funcCalls)
        
        if self.metric == Constant.WMC:
            fv = FunctionVisitor()
            fv.visit(node)
            self.res[node.name] = len(fv.functionList)



class Moose:
    def __init__(self, filePath):
        sourcecode = open(filePath)
        self.tree = ast.parse(sourcecode.read())
        sourcecode.close()

    def noc(self):
        classVisitor = ClassVisitor(Constant.NOC)
        classVisitor.visit(self.tree)
        return "NOC: " + str(classVisitor.res)

    def dit(self):
        classVisitor = ClassVisitor(Constant.DIT)
        classVisitor.visit(self.tree)
        return "DIT: " + str(classVisitor.res)

    def rfc(self):
        classVisitor = ClassVisitor(Constant.RFC)
        classVisitor.visit(self.tree)
        return "RFC: " + str(classVisitor.res)

    def wmc(self):
        classVisitor = ClassVisitor(Constant.WMC)
        classVisitor.visit(self.tree)
        return "WMC: " + str(classVisitor.res)


m = Moose("/home/anass/Documents/IAO/S3/GL/test.py")
# print(ast.dump(m.tree))
print(m.noc())
print()
print(m.dit())
print()
print(m.rfc())
print()
print(m.wmc())
